package view;

import java.util.Map;

import domain.Person;

public class PersonForm {

    private Person person;

    private Map<Integer, String> ageGroups;

    private String addContactButton;

    private String saveContactButton;

    private String pageStateKey;

    public Map<Integer, String> getAgeGroups() {
        return ageGroups;
    }

    public void setAgeGroups(Map<Integer, String> ageGroups) {
        this.ageGroups = ageGroups;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getAddContactButton() {
        return addContactButton;
    }

    public void setAddContactButton(String addContactButton) {
        this.addContactButton = addContactButton;
    }

    public String getPageStateKey() {
        return pageStateKey;
    }

    public void setPageStateKey(String pageStateKey) {
        this.pageStateKey = pageStateKey;
    }

    public String getSaveContactButton() {
        return saveContactButton;
    }

    public void setSaveContactButton(String saveContactButton) {
        this.saveContactButton = saveContactButton;
    }

}
