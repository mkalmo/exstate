package controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import view.PersonForm;
import dao.PersonDao;

@Controller
public class FlashPersonController {

    @Resource
    private PersonDao personDao;

    @RequestMapping("/flashform")
    public String showForm(@ModelAttribute("personForm") PersonForm form,
            ModelMap model) {

        model.addAttribute("url", "flashsave");
        form.setAgeGroups(personDao.getAgeGroups());

        return "form";
    }

    @RequestMapping(value = "/flashsave", method = RequestMethod.POST)
    public String saveForm(
            @ModelAttribute("personForm") PersonForm form) {

        return "redirect:/flashview";
    }

    @RequestMapping("/flashview")
    public String view(@ModelAttribute("personForm") PersonForm form) {

        return "view";
    }
}