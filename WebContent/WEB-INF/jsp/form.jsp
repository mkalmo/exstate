<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="styles.jsp"%>
</head>
<body>
<%@ include file="menu.jsp"%>

  ${message}<br />

  <c:if test="${empty url}">
      <c:set var="url" value="save"></c:set>
  </c:if>

  <c:url value="/${url}" var="theAction" />
  <form:form method="post" action="${theAction}" modelAttribute="personForm">
    <form:hidden path="pageStateKey" />
    <form:input type="hidden" path="person.id" />

    Eesnimi: <form:input id="firstName" path="person.firstName" /><br/>
    Perekonnanimi: <form:input id="name" path="person.name" /><br/>
    Vanusegrupp: <form:select id="ageGroupId"
                              path="person.ageGroupId"
                              items="${personForm.ageGroups}" />

    <br/><br/>

    <input type="submit" value="Salvesta" />

  </form:form>
</body>
</html>