package controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import view.PersonForm;
import dao.PersonDao;
import domain.Person;

@Controller
public class PersonController {

    @Resource
    private PersonDao personDao;

    @ModelAttribute("personForm")
    public PersonForm getUserObject() {
        return new PersonForm();
    }

    @RequestMapping("/")
    public String personList(ModelMap model) {
        List<Person> persons = personDao.findAll();

        model.addAttribute("persons", persons);

        return "list";
    }

    @RequestMapping("/delete/{personId}")
    public String deletePerson(@PathVariable("personId") Long personId) {
        personDao.delete(personId);

        return "redirect:/";
    }

    @RequestMapping("/edit/{personId}")
    public String editPerson(@PathVariable("personId") Long personId,
            @ModelAttribute("personForm") PersonForm form) {

        form.setPerson(personDao.findById(personId));
        form.setAgeGroups(personDao.getAgeGroups());

        return "form";
    }

    @RequestMapping("/addForm")
    public String showForm(ModelMap model,
            @ModelAttribute("personForm") PersonForm form,
            @RequestParam(value = "pageStateKey",
                          required = false) String pageStateKey) {

        form.setAgeGroups(personDao.getAgeGroups());

        return "form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveForm(HttpSession session, ModelMap model,
            @ModelAttribute("personForm") PersonForm form) {

        form.setAgeGroups(personDao.getAgeGroups());

        personDao.store(form.getPerson());

        return "redirect:/view";
    }

    @RequestMapping("/view")
    public String view(@ModelAttribute("personForm") PersonForm form) {

        return "view";
    }

}